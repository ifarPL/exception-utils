/*
Copyright (C) 2018-2019 Rafał Szewczyk <admin@rafsze.pl>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

package pl.rafsze.utils.exceptions;

/**
 * Utility that allow raising checked exceptions without try or throws clause.
 * 
 * @author Rafał Szewczyk <admin@rafsze.pl>
 */
public final class SneakyThrow
{
	private SneakyThrow()
	{
	}
	
	/**
	 * Throws any exception as unchecked one.
	 * It's useful when there is a need to throw checked exception like IOException, 
	 * but the method signature does not allow it e.g. when implementing the Runnable interface.
	 * 
	 * @param exception to throw
	 * @return This method actually never returs normally - an exception always is thrown.
	 */
	public static <T> T raise(Throwable exception)
	{
		SneakyThrow.<RuntimeException>doSneakyThrow(exception);
		return null;
	}

	@SuppressWarnings("unchecked")
	private static <T extends Throwable> void doSneakyThrow(Throwable exception) throws T
	{
		throw (T) exception;
	}
}
