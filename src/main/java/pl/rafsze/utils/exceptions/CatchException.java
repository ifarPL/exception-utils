/*
Copyright (C) 2018-2019 Rafał Szewczyk <admin@rafsze.pl>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

package pl.rafsze.utils.exceptions;

import java.util.function.Consumer;

/**
 * An utility class that makes catching exceptions simpler, especially in lambda expressions.
 * 
 * @author Rafał Szewczyk <admin@rafsze.pl>
 */
public class CatchException
{
	@FunctionalInterface
	public interface ExceptionRunnable
	{
		void run() throws Throwable;
	}
	
	@FunctionalInterface
	public interface ExceptionSupplier <T>
	{
		T get() throws Throwable;
	}
	
	public static void with(ExceptionRunnable runnable, Consumer<Throwable> exceptionHandler)
	{
		Check.notNull(runnable, "Runnable cannot be null");
		Check.notNull(exceptionHandler, "Exception handler cannot be null");
		
		try
		{
			runnable.run();
		}
		catch ( Throwable e )
		{
			exceptionHandler.accept(e);
		}
	}
	
	public static <T> T with(ExceptionSupplier<? extends T> supplier, Consumer<Throwable> exceptionHandler, T defaultValue)
	{
		Check.notNull(supplier, "Supplier cannot be null");
		Check.notNull(exceptionHandler, "Exception handler cannot be null");
		
		try
		{
			return supplier.get();
		}
		catch ( Throwable e )
		{
			exceptionHandler.accept(e);
			return defaultValue;
		}
	}
	
	public static void printStackTrace(ExceptionRunnable runnable)
	{
		CatchException.with(runnable, Throwable::printStackTrace);
	}
	
	public static <T> T printStackTrace(ExceptionSupplier<? extends T> supplier, T defaultValue)
	{
		return CatchException.with(supplier, Throwable::printStackTrace, defaultValue);
	}
	
	public static void sneaky(ExceptionRunnable runnable)
	{
		CatchException.with(runnable, SneakyThrow::raise);
	}	
	
	public static <T> T sneaky(ExceptionSupplier<T> supplier)
	{
		return CatchException.with(supplier, SneakyThrow::raise, null);
	}
}
