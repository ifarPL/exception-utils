/*
Copyright (C) 2018-2019 Rafał Szewczyk <admin@rafsze.pl>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

package pl.rafsze.utils.exceptions;

import java.util.function.Supplier;

/**
 * A helper class for performing runtime assertions.
 * 
 * @author Rafał Szewczyk <admin@rafsze.pl>
 */
public final class Check
{
	private Check()
	{
	}
	
	public static void state(boolean expression) throws IllegalStateException
	{
		if ( !expression )
		{
			throw new IllegalStateException();
		}
	}
	
	public static void state(boolean expression, String message) throws IllegalStateException
	{
		if ( !expression )
		{
			throw new IllegalStateException(message);
		}
	}
	
	public static void state(boolean expression, Supplier<String> message) throws IllegalStateException
	{
		if ( !expression )
		{
			throw new IllegalStateException(message != null ? message.get() : null);
		}
	}
	
	public static void argument(boolean expression) throws IllegalArgumentException
	{
		if ( !expression )
		{
			throw new IllegalArgumentException();
		}
	}
	
	public static void argument(boolean expression, String message) throws IllegalArgumentException
	{
		if ( !expression )
		{
			throw new IllegalArgumentException(message);
		}
	}
	
	public static void argument(boolean expression, Supplier<String> message) throws IllegalArgumentException
	{
		if ( !expression )
		{
			throw new IllegalArgumentException(message != null ? message.get() : null);
		}
	}
	
	public static <T> T notNull(T object) throws NullPointerException
	{
		if ( object == null )
		{
			throw new NullPointerException();
		}
		
		return object;
	}
	
	public static <T> T notNull(T object, String message) throws NullPointerException
	{
		if ( object == null )
		{
			throw new NullPointerException(message);
		}
		
		return object;
	}
	
	public static <T> T notNull(T object, Supplier<String> message) throws NullPointerException
	{
		if ( object == null )
		{
			throw new NullPointerException(message != null ? message.get() : null);
		}
		
		return object;
	}
	
	public static int index(int index, int minInclusive, int maxInclusive) throws IndexOutOfBoundsException
	{
		if ( index < minInclusive || index > maxInclusive )
		{
			throw new IndexOutOfBoundsException();
		}
		
		return index;
	}
	
	public static int index(int index, int minInclusive, int maxInclusive, String message) throws IndexOutOfBoundsException
	{
		if ( index < minInclusive || index > maxInclusive )
		{
			throw new IndexOutOfBoundsException(message);
		}
		
		return index;
	}
	
	public static int index(int index, int minInclusive, int maxInclusive, Supplier<String> message) throws IndexOutOfBoundsException
	{
		if ( index < minInclusive || index > maxInclusive )
		{
			throw new IndexOutOfBoundsException(message != null ? message.get() : null);
		}
		
		return index;
	}
}
